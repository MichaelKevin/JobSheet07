package jobsheet07;
import java.util.Scanner;
public class Lat1 {
    public static void main(String[] args) {
        int a;
        int nilai;
        String identitas = "Michael Kevin / XRPL6 / 30";
        String grade;
        System.out.println("Identitas : " + identitas);
        
        //Commit Message : Initial Commit.
        Scanner masuk = new Scanner(System.in);
        System.out.print("Nilai a = ");
        a = masuk.nextInt();
        
        System.out.println("\nIF.. 1");
        if(a < 5)
            System.out.println("Nilai a kurang dari 5");
        
        System.out.println("\nIF.. 2");
        if(a == 5)
            System.out.println("Nilai a sama dengan 5");
        
        System.out.println("\nIF.. 3");
        if(a > 5){
            System.out.println("Isi variabel a : " + a);
            System.out.println("Nilai a lebih dari 5");
        }
        //Commit Message : Coba IF.
        System.out.println("\nIF..ELSE..");
        if(a < 5){
            System.out.println("Isi variabel a : " + a);
            System.out.println("Nilai a kurang dari 5");
        }else
            System.out.println("Nilai a lebih dari atau sama dengan 5");
        
        //Commit Message : Coba IF..ELSE.
        System.out.println("\nIF..ELSE.. IF..ELSE.. 1");
        if(a < 5){
            System.out.println("Nilai a kurang dari 5");
       }else if(a == 5){
           System.out.println("Nilai a sama dengan 5");
       }else
           System.out.println("Nilai a lebih dari 5");
        
        System.out.println("\nIF..ELSE.. IF..ELSE.. 2");
        if(a < 2){
            System.out.println("NIlai a kurang dari 2");
        }else if(a < 4){
            System.out.println("NIlai a kurang dari 4");
        }else if(a < 6){
            System.out.println("NIlai a kurang dari 6");
        }else if(a == 6){
            System.out.println("NIlai a sama dengan 6");
        }else
            System.out.println("NIlai a lebih dari 6");
        
        //Commit Message : Coba IF..ELSE IF..ELSE.
        System.out.println("\nNestedIF");
        if(a < 7){
            System.out.println("Nilai a kurang dari 7");
            if(a > 2)
                System.out.println("Niali a lebih dari 2");
            if(a < 4)
                System.out.println("Nilai kurang dari 4");
        }
        
        //Commit Message : Coba Nested IF
        System.out.println("\nSwitch Case");
        switch(a){
            case 1 : System.out.println("Nilai a => 1");
            break;
            case 2 : System.out.println("Nilai a => 2");
            break;
            case 3 : 
            case 4 : System.out.println("Nilai a => 3 atau 4");
            break;
            case 5 : System.out.println("Nilai a => 5");
            break;
            default :System.out.println("Nilai a bukan antar 1-5");
    }
        //Commit Message : Coba SWITCH..CASE
        System.out.print("Nilai Anda = ");
        nilai = masuk.nextInt();
        grade = (nilai < 75) ? "Belum kompeten" : "Kompeten";
        System.out.println(nilai + " => " + grade);
    }  
}         
